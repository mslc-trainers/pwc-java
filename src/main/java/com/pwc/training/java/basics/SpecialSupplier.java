package com.pwc.training.java.basics;

import com.pwc.training.java.assignment1.Supplier;

public class SpecialSupplier extends Supplier {


    /**
     * Protected can be accessed from out of the package only in classes
     * that are inherited
     */
    public void displayAllDetails() {
        super.displayName();

    }
}
