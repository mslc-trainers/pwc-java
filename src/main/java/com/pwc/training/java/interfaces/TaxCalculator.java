package com.pwc.training.java.interfaces;


/**
 * new features added to interfaces in java 1.8
 */

public interface TaxCalculator {

    /**
     *
     */
    public float calculateTax(String state, float baseTax);
//    public float calculateStateTax();



    /**
     * In order to provide default implementation in set of classes
     *
     * @return
     */
    public default float getBaseTax() {

        return 2.3f;
    }

    /**
     * used for writing utility methods
     *
     * @return
     */
    public static float getDefaultTax() {

        return 5.5f;
    }

//    final int defaultTax = 25;


}
