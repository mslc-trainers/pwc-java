package com.pwc.training.java.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class PreparedStatementDemo {

    public static void main(String[] args) throws Exception {

        Connection con = JDBConnectionHelper.getConnection();

        int id = 102;
        String addressCondition = "Mumbai";
//        Statement stmt = con.createStatement();

        /**
         * retrieved based on ID (primary key) - Just 1 Row that too if it exists
         *
         * Now change the select statement to retrieve all rows where address = Mumbai
         * and display the same. If there are no rows where address = Mumbai then
         * simply display a message "No rows found with address = "Mumbai"
         *
         */
        PreparedStatement preparedStatement =
                con.prepareStatement("Select  * from Customer where id = ?");


        preparedStatement.setInt(1, id);

        ResultSet rs = preparedStatement.executeQuery();

        boolean rowFound = rs.next();
        if (rowFound) {

            String name = rs.getString("name");
            String address = rs.getString("address");
            System.out.println(name + " -- " + address);
        } else {
            System.out.println("The row with id : " + id + " not found");
        }


    }
}
