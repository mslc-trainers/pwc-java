package com.pwc.training.java.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertDemo {

    public static void main(String[] args) throws SQLException {


        Connection con = DriverManager
                .getConnection("jdbc:mysql://192.168.29.83:3306/training",
                        "user", "user");

        Statement stmt = con.createStatement();

        stmt.execute("Insert into Customer (id, name, address) values (104, 'Verizon', 'Chennai')");


    }
}
