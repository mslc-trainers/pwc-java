package com.pwc.training.java.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class UpdateDemo {

    public static void main(String[] args) throws SQLException {


        Connection con = DriverManager
                .getConnection("jdbc:mysql://192.168.29.83:3306/training",
                        "user", "user");

        Statement stmt = con.createStatement();

        stmt.execute("Update  Customer set address = 'Hyderabad' where id = 101");



    }
}
