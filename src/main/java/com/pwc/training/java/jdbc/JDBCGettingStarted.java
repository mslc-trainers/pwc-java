package com.pwc.training.java.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class JDBCGettingStarted {


    public static void main(String[] args) throws Exception {

        List<Integer> values = new LinkedList<>();


        //Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/training");
        Connection con = DriverManager
                .getConnection("jdbc:mysql://192.168.29.83:3306/training",
                        "user", "user");

        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery("Select * from Customer");


        while (rs.next()) {

            /**
             *
             * id, name, address
             */
            int id = rs.getInt("id");
            String name = rs.getString("name");
            String address = rs.getString("address");

            System.out.println(id + " -- " + name + " -- " + address);


        }


    }
}
