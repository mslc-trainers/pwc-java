package com.pwc.training.java.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public interface JDBConnectionHelper {

    static Connection getConnection() throws Exception {

        Connection con = DriverManager
                .getConnection("jdbc:mysql://192.168.29.83:3306/training",
                        "user", "user");


        return con;


    }
}
