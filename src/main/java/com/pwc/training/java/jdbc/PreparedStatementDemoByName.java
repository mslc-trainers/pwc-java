package com.pwc.training.java.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class PreparedStatementDemoByName {

    public static void main(String[] args) throws Exception {

        Connection con = JDBConnectionHelper.getConnection();


        String addressCondition = "Pune";
//        Statement stmt = con.createStatement();

        /**
         * retrieved based on ID (primary key) - Just 1 Row that too if it exists
         *
         * Now change the select statement to retrieve all rows where address = Mumbai
         * and display the same. If there are no rows where address = Mumbai then
         * simply display a message "No rows found with address = "Mumbai"
         *
         */
        PreparedStatement preparedStatement =
                con.prepareStatement("Select  * from Customer where address = ?");


        preparedStatement.setString(1, addressCondition);

        ResultSet rs = preparedStatement.executeQuery();

        boolean rowFound = rs.next();
        if (rowFound) {
            String name0 = rs.getString("name");
            String address0 = rs.getString("address");
            System.out.println(name0 + " -- " + address0);

            while (rs.next()) {
                String name = rs.getString("name");
                String address = rs.getString("address");
                System.out.println(name + " -- " + address);
            }
        } else {
            System.out.println("The row with address : " + addressCondition + " not found");
        }


    }
}
