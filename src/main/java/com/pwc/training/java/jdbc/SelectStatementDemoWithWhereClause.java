package com.pwc.training.java.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SelectStatementDemoWithWhereClause {

    public static void main(String[] args) throws Exception {

        Connection con = JDBConnectionHelper.getConnection();

        int id = 105;
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery("Select  * from Customer where id = " + id);
        boolean rowFound = rs.next();
        if (rowFound) {

            String name = rs.getString("name");
            String address = rs.getString("address");
            System.out.println(name + " -- " + address);
        } else {
            System.out.println("The row with id : " + id + " not found");
        }


    }
}
