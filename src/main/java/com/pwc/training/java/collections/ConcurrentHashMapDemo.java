package com.pwc.training.java.collections;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapDemo {

    public static void main(String[] args) {

        // Not Thread Safe (1.2)
        HashMap<Integer, String> hashMap = new HashMap<>();

        // Thread Safe (1.0)
        /**
         * One thread at a time will be able write
         */
        Hashtable<Integer, String> map2 = new Hashtable<>();


//        Thread Safe (1.5)
        /**
         * Writes can happen (16 writes can happen concurrently)
         * All the reads can happen concurrently
         * Lock Stripping
         *
         */
        ConcurrentHashMap<Integer, String> map = new ConcurrentHashMap<>();


    }
}
