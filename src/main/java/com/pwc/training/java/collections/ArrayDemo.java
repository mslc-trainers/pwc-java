package com.pwc.training.java.collections;


public class ArrayDemo {

    public static void main(String[] args) {


        int[] numbers = {1, 2, 5, 6};
        int[] numbers2 = new int[10];
        numbers2[0] = 323;
        numbers2[1] = 45;


        String[] names = {"name1", "name2", "name3"};

        System.out.println(numbers[0] + " -- " + numbers.length);


        System.out.println(names[1]);


    }
}
