package com.pwc.training.java.synchronization;


import java.util.concurrent.atomic.AtomicInteger;

public class SynchronizeDemo {

    volatile int i = 5;

    public static void main(String[] args) {

        SynchronizeDemo d = new SynchronizeDemo();
        d.i = 10;
        // Not atomic
        if (d.i == 10) {
            d.i = d.i + 15;
        }

        AtomicInteger i = new AtomicInteger(10);
        int newValue = i.incrementAndGet();


    }

}

class MyInteger {

    private int value;

    /**
     * a) No two threads will be able to access this function at the same point in time
     * b) The moment sync block starts, JVM read barrier & when it leaves the sync
     * - it executes the write barrier (aka memory fences)
     *
     * @param value
     */
    public synchronized void setValue(int value) {
        this.value = value;
    }

    public synchronized void increment() {
        value = value + 1;
    }
}
