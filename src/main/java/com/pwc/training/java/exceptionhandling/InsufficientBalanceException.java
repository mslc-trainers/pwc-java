package com.pwc.training.java.exceptionhandling;

public class InsufficientBalanceException extends Exception {


    public InsufficientBalanceException() {

    }

    public InsufficientBalanceException(String message) {
        super(message);

    }


}
