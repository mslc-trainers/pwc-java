package com.pwc.training.java.exceptionhandling;

public class CreditLimitReachedException extends Exception {

    public CreditLimitReachedException(String message) {
        super(message);

    }
}
