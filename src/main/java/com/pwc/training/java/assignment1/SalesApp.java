package com.pwc.training.java.assignment1;

public class SalesApp {

    /**
     * Clients being dependent on Abstraction and not on the implementation
     * (Factory Pattern)
     *
     * @param args
     */
    public static void main(String[] args) {

        Customer aCustomer = new LocalCustomer();
        aCustomer.displayName();

        Supplier supplier = new Supplier();
        supplier.displayName();


        aCustomer.createInvoice();

        printCustomerDetails(aCustomer);

    }


    /**
     * <p>
     * printCustomerDetails is neither dependent on LocalCustomer
     * nor it is dependent on ExportCustomer
     * </p>
     * <p>
     * In fact it is agnostic of the existence of LocalCustomer and ExportCustomer
     * </p>
     *
     * @param customer
     */
    public static void printCustomerDetails(Customer customer) {

        customer.printDetails();


    }

}
