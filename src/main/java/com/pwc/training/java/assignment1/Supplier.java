package com.pwc.training.java.assignment1;

public class Supplier implements Comparable<Supplier> {


    private Integer id;
    private String name;
    private String city;


    public Supplier() {


    }

    public Supplier(int id) {
        this.id = id;
    }

    protected void displayName() {

    }

    @Override
    public int compareTo(Supplier o) {

//        return o.id.compareTo(this.id);

        return this.id.compareTo(o.id);
    }

    @Override
    public String toString() {
        return "Supplier{" +
                "id=" + id +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
