package com.pwc.training.java.assignment1;

import java.util.Comparator;

public class SupplierComparatorByName implements Comparator<Supplier> {
    @Override
    public int compare(Supplier o1, Supplier o2) {


        return o1.getName().compareTo(o2.getName());

    }
}
