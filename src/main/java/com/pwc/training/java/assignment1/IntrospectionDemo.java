package com.pwc.training.java.assignment1;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class IntrospectionDemo {


    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, InvocationTargetException {


        Customer aCustomer = new ExportCustomer();

        printEmployeeDetails(aCustomer);


        String className = "com.pwc.training.java.assignment1.Customer";

        Class c = Class.forName(className);

        Method[] methods = c.getDeclaredMethods();

        for (Method m : methods) {
            System.out.println(m.getName());
        }
        // Introspect on fields and print me the names of the fields

        Field[] fields = c.getDeclaredFields();

        for (Field f : fields) {
            System.out.println(f.getName());
        }


    }

    /**
     * This function is depdendent on abstraction and not on Implementation
     * @param customer
     */
    public static void printEmployeeDetails(Customer customer) {

        customer.printDetails();
        ;

    }
}
