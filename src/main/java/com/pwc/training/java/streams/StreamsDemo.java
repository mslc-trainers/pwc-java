package com.pwc.training.java.streams;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class StreamsDemo {

    public static void main(String[] args) {

//        List<Integer> values = new ArrayList<>();
//        values.add(123);
//        values.add(323);

        List<Integer> values = Arrays.asList(12, 40, 45, 75, 100, 200);

        /**
         * filter out all those elements which are greater than 50
         *
         */

//        Predicate<Integer> testForValueGreaterThan50 = new Predicate<Integer>() {
//            @Override
//            public boolean test(Integer value) {
//
//                if (value > 50) {
//                    return true;
//                } else {
//                    return false;
//                }
//            }
//        } ;

//        Predicate<Integer> testForValueGreaterThan50 = value -> value > 50;


        // Filtering
        Predicate<Integer> filterPredicate = (value) -> value < 15;
        List<Integer> filteredValues = filterMyValues(values, filterPredicate);
        System.out.println(filteredValues);

        // Mapping

        Function<Integer, String> mapFunction = x -> "Employee Code : " + x;
        List<String> mappedValues = mapMyValues(values, mapFunction);
        System.out.println(mappedValues);


        OptionalInt minValue = values
                .stream()
                .mapToInt(x -> x.intValue())
                .max();

        if (minValue.isPresent()) {
            System.out.println(minValue.getAsInt());
        }

        Set<String> mappedAndFilteredValues =
                values.stream()
                        .filter(x -> x < 15)
                        .map(x -> "Employee Id : " + x)
                        .collect(Collectors.toCollection(TreeSet::new));

        int theSum = values
                .stream()
                .reduce((x, y) -> x + y)
                .get();

        System.out.println(" Sum : " + theSum);


        values
                .stream()
                .mapToInt(x -> x.intValue())
                .sum();


        System.out.println("---------");
        System.out.println(mappedAndFilteredValues);

        /**
         *
         * Following are the most common functional interfaces
         *
         */

        Function<Integer, Integer> f1 = null;
        Consumer<String> f2 = null;
        Supplier<Integer> f3 = null;
        Predicate<Integer> f4 = null;


    }

    public static List<Integer> filterMyValues(List<Integer> values, Predicate<Integer> predicate) {
        List<Integer> filteredValues =
                values
                        .stream()
                        // higher order function is filter
                        .filter(predicate)
                        .collect(Collectors.toList());

        return filteredValues;

    }

    /**
     * You must map the List of Integer to List of String with each value in the list of string concatenated
     * with "Employee Code : " + what ever the code
     *
     * @param values
     * @param mapFunction
     * @return
     */
    public static List<String> mapMyValues(List<Integer> values, Function<Integer, String> mapFunction) {


        List<String> mappedList =
                values
                        .stream()
                        // higher order function is map
                        .map(mapFunction)
                        .collect(Collectors.toList());

        return mappedList;

    }
}
