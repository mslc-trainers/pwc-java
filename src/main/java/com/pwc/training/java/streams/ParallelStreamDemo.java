package com.pwc.training.java.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ParallelStreamDemo {

    public static void main(String[] args) {


        List<Integer> values = Arrays.asList(12, 40, 45, 75, 100, 200);

        long start = System.currentTimeMillis();

        List<Integer> filteredValues = values
                .stream()
//                .parallel()
                .filter(x -> {

                    System.out.println(x + " is the element that is processed in : " + Thread.currentThread().getName());

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                    return x.intValue() > 40;
                })
                .collect(Collectors.toList());

        long end = System.currentTimeMillis();


        System.out.println("time taken is : " + (end - start) + filteredValues);


    }
}
